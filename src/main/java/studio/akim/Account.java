package studio.akim;

import java.util.UUID;

class Account {
    static final Integer DEFAULT_MONEY_VALUE = 10000;

    private String id;
    private Integer money;

    private Account(String id, Integer money) {
        this.id = id;
        this.money = money;
    }

    Account() {
        this(UUID.randomUUID().toString(), DEFAULT_MONEY_VALUE);
    }

    Integer getMoney() {
        return money;
    }

    String getId() {
        return id;
    }

    synchronized void increaseMoney(Integer money) {
        this.money += money;
    }

    synchronized void decreaseMoney(Integer money) {
        if (this.money < money) {
            throw new NotEnoughMoneyException();
        }
        this.money -= money;
    }
}
