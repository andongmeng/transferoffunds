package studio.akim;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    private final static Logger log = LogManager.getLogger(Main.class);

    private static final Integer ACCOUNT_COUNT = 4;
    private static final Integer THREAD_COUNT = 2;
    private static final Integer TRANSACTION_LIMIT = 30;

    public static void main(String[] args) {
        AtomicInteger transactionCount = new AtomicInteger();
        Account[] accounts = new Account[ACCOUNT_COUNT];
        for (int i = 0; i < accounts.length; i++) {
            accounts[i] = new Account();
            log.info("Account with ID \"" + accounts[i].getId() + "\" created");
        }
        for (int i = 0; i < THREAD_COUNT; i++) {
            new Thread(() -> {
                while (transactionCount.get() < TRANSACTION_LIMIT) {
                    transactionCount.incrementAndGet();
                    try {
                        Thread.sleep(1000 + ThreadLocalRandom.current().nextInt(1001));

                        int transferSum = 1 + ThreadLocalRandom.current().nextInt(Account.DEFAULT_MONEY_VALUE);
                        int accountFromIndex = ThreadLocalRandom.current().nextInt(accounts.length);
                        int accountToIndex = ThreadLocalRandom.current().nextInt(accounts.length);
                        while (accountToIndex == accountFromIndex) {
                            accountToIndex = ThreadLocalRandom.current().nextInt(accounts.length);
                        }

                        int accountFirstLockIndex = Math.min(accountFromIndex, accountToIndex);
                        int accountSecondLockIndex = Math.max(accountFromIndex, accountToIndex);

                        try {
                            synchronized (accounts[accountFirstLockIndex]) {
                                synchronized (accounts[accountSecondLockIndex]) {
                                    accounts[accountFromIndex].decreaseMoney(transferSum);
                                    accounts[accountToIndex].increaseMoney(transferSum);
                                    log.info("Transfer " + transferSum + " from \"" + accounts[accountFromIndex].getId() + "\" to \""
                                            + accounts[accountToIndex].getId() + "\" successful");
                                }
                            }
                        } catch (NotEnoughMoneyException e) {
                            log.warn("Transfer " + transferSum + " from \"" + accounts[accountFromIndex].getId() + "\" to \""
                                    + accounts[accountToIndex].getId() + "\" failed due to lack of funds");
                            transactionCount.decrementAndGet();
                        }
                    } catch (InterruptedException e) {
                        log.error("Thread is interrupted");
                        log.debug(e.getStackTrace());
                    }
                }
            }).start();
        }
    }
}
